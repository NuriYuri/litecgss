#ifndef CGSS_WINDOW_SETTINGS_CHANGE_EVENT_H
#define CGSS_WINDOW_SETTINGS_CHANGE_EVENT_H

namespace cgss {
	struct WindowSizeChangeEvent {
		unsigned int width;
		unsigned int height;
	};
}

#endif