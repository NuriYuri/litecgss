#ifndef CGSS_DISPLAY_WINDOW_CONTEXT_SETTINGS_H
#define CGSS_DISPLAY_WINDOW_CONTEXT_SETTINGS_H

#include <SFML/Window/ContextSettings.hpp>

namespace cgss {
	using DisplayWindowContextSettings = sf::ContextSettings;
}

#endif