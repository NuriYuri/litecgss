#ifndef CGSS_FONT_H
#define CGSS_FONT_H

#include <SFML/Graphics/Font.hpp>

namespace cgss {
	using Font = sf::Font;
}

#endif