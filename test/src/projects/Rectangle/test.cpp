#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "Common/Rectangle.h"

struct BindableToRectangle :
	public cgss::Bindable<sf::IntRect> {
	MOCK_METHOD(void, updateFromValue, (const sf::IntRect* ptr), (override));
	const sf::IntRect* getValue() const { return cgss::Bindable<sf::IntRect>::getValue(); }
};

TEST(RectangleGroup, Init) {
	auto rectangle = cgss::Rectangle {};
	const auto defaultRect = sf::IntRect{};
	EXPECT_EQ(static_cast<const cgss::Rectangle&>(rectangle).getValue(), defaultRect);
}

TEST(RectangleGroup, SetX) {
	auto rectangle = cgss::Rectangle{};

	rectangle.setX(1);
	EXPECT_EQ(static_cast<const cgss::Rectangle&>(rectangle).getValue(), (sf::IntRect{1, 0, 0, 0}));
}

TEST(RectangleGroup, SetY) {
	auto rectangle = cgss::Rectangle{};

	rectangle.setY(1);
	EXPECT_EQ(static_cast<const cgss::Rectangle&>(rectangle).getValue(), (sf::IntRect{0, 1, 0, 0}));
}

TEST(RectangleGroup, SetWidth) {
	auto rectangle = cgss::Rectangle{};

	rectangle.setWidth(1);
	EXPECT_EQ(static_cast<const cgss::Rectangle&>(rectangle).getValue(), (sf::IntRect{0, 0, 1, 0}));
}

TEST(RectangleGroup, SetHeight) {
	auto rectangle = cgss::Rectangle{};

	rectangle.setHeight(1);
	EXPECT_EQ(static_cast<const cgss::Rectangle&>(rectangle).getValue(), (sf::IntRect{0, 0, 0, 1}));
}

TEST(RectangleGroup, BindableSetX) {
	auto rectangle = cgss::Rectangle{};
	auto bindable = BindableToRectangle{};
	rectangle.bind(&bindable);
	EXPECT_CALL(bindable, updateFromValue(&static_cast<const cgss::Rectangle&>(rectangle).getValue())).Times(1);
	rectangle.setX(1);
	EXPECT_EQ(*static_cast<const BindableToRectangle&>(bindable).getValue(), (sf::IntRect{1, 0, 0, 0}));
}

TEST(RectangleGroup, BindableSetY) {
	auto rectangle = cgss::Rectangle{};
	auto bindable = BindableToRectangle{};
	rectangle.bind(&bindable);
	EXPECT_CALL(bindable, updateFromValue(&static_cast<const cgss::Rectangle&>(rectangle).getValue())).Times(1);
	rectangle.setY(1);
	EXPECT_EQ(*static_cast<const BindableToRectangle&>(bindable).getValue(), (sf::IntRect{0, 1, 0, 0}));
}

TEST(RectangleGroup, BindableSetWidth) {
	auto rectangle = cgss::Rectangle{};
	auto bindable = BindableToRectangle{};
	rectangle.bind(&bindable);
	EXPECT_CALL(bindable, updateFromValue(&static_cast<const cgss::Rectangle&>(rectangle).getValue())).Times(1);
	rectangle.setWidth(1);
	EXPECT_EQ(*static_cast<const BindableToRectangle&>(bindable).getValue(), (sf::IntRect{0, 0, 1, 0}));
}

TEST(RectangleGroup, BindableSetHeight) {
	auto rectangle = cgss::Rectangle{};
	auto bindable = BindableToRectangle{};
	rectangle.bind(&bindable);
	EXPECT_CALL(bindable, updateFromValue(&static_cast<const cgss::Rectangle&>(rectangle).getValue())).Times(1);
	rectangle.setHeight(1);
	EXPECT_EQ(*static_cast<const BindableToRectangle&>(bindable).getValue(), (sf::IntRect{0, 0, 0, 1}));
}
